# copyright 2014 Jezra
# released under GPLv3
# see: http://www.gnu.org/licenses/gpl-3.0.html

import re

class SMNProcessor:
	def __init__(self):
		pass

	def set_tempo(self, tempo):
		self.tempo = tempo
		self.beat_length = 60.0/tempo

	def get_note_duration(self, string):
		duration = 0
		bits = string.split("-")
		for bit in bits:
			try:
				modifier = float(bit)
				duration += self.beat_length / modifier
			except:
				print "invalid duration for "+ string
		return duration

	def process(self, text):
		self.tempo = 60
		self.beat_length = 1
		melody = []

		#remove #lines
		text = re.sub(r"#.*", "", text)

		#replace multiple whitespaces with a sace
		text = re.sub(r"\s+", " ", text)

		#split the text into an array
		bits = text.split(" ")
		for bit in bits:
			if bit != '':
				char = bit[0]
				#is there more than just the char?
				if len(bit) > 1:
					time = bit[1:]
				else:
					time = "1"
				if char == 't':
					self.set_tempo( int(time) )
				elif char in ['G','a','b','c','d','e','f','g']:
					pitch = char
					duration = self.get_note_duration(time)
					melody.append( [pitch, duration] )
		return melody
