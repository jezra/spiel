# copyright 2014 Jezra
# released under GPLv3
# see: http://www.gnu.org/licenses/gpl-3.0.html


#!/usr/bin/env python2

import os, signal, time
from SMNProcessor import SMNProcessor
from IOPin import IOPin

#where is this script?
script_dir = os.path.dirname( os.path.realpath(__file__) )
music_dir = os.path.join(script_dir, 'SMN')
input_file = os.path.join(script_dir, 'play_smn')

class Spiel:
	def __init__(self):
		self.processor = SMNProcessor()
		#build the pitch iopins
		self.pitch = {}
		self.pitch["g"] = IOPin("P9_11", "out")
		self.play_pitch("g")
		self.pitch["f"] = IOPin("P9_12", "out")
		self.play_pitch("f")
		self.pitch["e"] = IOPin("P9_13", "out")
		self.play_pitch("e")
		self.pitch["d"] = IOPin("P9_14", "out")
		self.play_pitch("d")
		self.pitch["c"] = IOPin("P9_15", "out")
		self.play_pitch("c")
		self.pitch["b"] = IOPin("P9_16", "out")
		self.play_pitch("b")
		self.pitch["a"] = IOPin("P8_7", "out")
		self.play_pitch("a")
		self.pitch["G"] = IOPin("P8_8", "out")
		self.play_pitch("G")

	def quit(self, signum = None, frame=None):
		self.looping = False

	def run(self):
		#prepare to catch ctrl+c
		signal.signal(signal.SIGINT, self.quit)
		#start looping
		self.looping = True
		print "starting main loop... \n"
		while self.looping:
			#read an input file for a .smn to play
			if os.path.isfile(input_file):
				f = open(input_file, 'r')
				name = f.readline().strip()
				#close and remove the file
				f.close()
				os.remove( input_file )

				# does the file exist?
				smn_file = os.path.join(music_dir,name)
				if os.path.isfile( smn_file ):
					#read the smn file
					f = open(smn_file, 'r')
					data = f.read().strip()
					#close and remove the file
					f.close()
					melody = self.processor.process( data )
					self.play_melody(melody)
				else:
					print smn_path, "does not exist\n"

			time.sleep(0.1)

	def play_melody(self, melody):
		for note in melody:
			self.play_pitch( note[0] )
			time.sleep( note[1] )

	def play_pitch(self, pitch):
		print pitch
		self.pitch[pitch].low()
		time.sleep(0.02)
		self.pitch[pitch].high()



if __name__ == "__main__":
	spiel = Spiel()
	#start the spiel running
	spiel.run()
