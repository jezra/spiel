#!/usr/bin/env python2
import getpass
import sys
from Spiel import Spiel
#is the user root?
if getpass.getuser() != 'root':
	print "\n--- This script must be run as root ---\n"
	print "you are: ", getpass.getuser()
	sys.exit()

#create a spiel
spiel = Spiel()
#start the spiel running
spiel.run()
