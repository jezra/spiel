#!/usr/bin/env python2

import os, signal, time

from IOPin import IOPin

class Heat:
	def __init__(self):
		#build the pitch iopins
		self.pitch = {}
		#self.pitch["g"] = IOPin("P9_11", "out")
		#self.pitch["f"] = IOPin("P9_12", "out")
		#self.pitch["e"] = IOPin("P9_13", "out")
		#self.pitch["d"] = IOPin("P9_14", "out")
		self.pitch["c"] = IOPin("P9_15", "out")
		#self.pitch["b"] = IOPin("P9_16", "out")
		#self.pitch["a"] = IOPin("P8_7", "out")
		#self.pitch["G"] = IOPin("P8_8", "out")


	def quit(self, signum = None, frame=None):
		self.looping = False

	def run(self):
		#prepare to catch ctrl+c
		signal.signal(signal.SIGINT, self.quit)
		#start looping
		self.looping = True
		#turn on the pitches
		for pitch in self.pitch:
			self.pitch[pitch].low()
		print "starting main loop... \n"
		while self.looping:
			time.sleep(0.1)



if __name__ == "__main__":
	heat = Heat()
	#start the heat running
	heat.run()
