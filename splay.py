#!/usr/bin/env python2
import os

smn_dir = "SMN"
#get a list of the files in SMN
files = [ f for f in os.listdir(smn_dir) if os.path.isfile(os.path.join(smn_dir,f)) ]
#display to the user with a number
valid_input = False
while not valid_input:
	print "-- Files --"
	for i in range(len(files)):
		f = files[i]
		print "%d: %s" % (i+1,f)
	#get user feedback
	print"\nEnter the number of the file you would like to play, or 'q' to quit"
	number = raw_input()
	if number == 'q':
		valid_input = True
	else:
		number = int(number)
	if number > 0 and number <= len(files) :
		valid_input = True
		fh = open("play_smn",'w')
		fh.write(files[number-1])
		fh.close()
