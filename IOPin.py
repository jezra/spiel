# copyright 2014 Jezra
# released under GPLv3
# see: http://www.gnu.org/licenses/gpl-3.0.html

# we need the Adafruit BBB library for GPIO
import Adafruit_BBIO.GPIO as GPIO

class IOPin:
	def __init__(self, name, direction):
		self.name = name
		self.direction = direction
		if direction == "out":
			d = GPIO.OUT
		elif direction == "in":
			d = GPIO.IN
		else:
			raise ValueError ("'direction' must be either 'out' or 'in'")
		#enable the pin
		GPIO.setup(name, d)
		print "Setting "+name+" as "+str(d)
		if d == GPIO.IN:
			print "initial value: " + str(GPIO.input(self.name))

		#set some defaults for pin inputs
		self.old_value = None
		self.current_value = self.value()

	def on (self):
		self.high()

	def off (self):
		self.low()

	def low(self):
		GPIO.output(self.name, 0)

	def high(self):
		GPIO.output(self.name, 1)

	def value(self):
		return GPIO.input(self.name)

	#check if there has been a change in value
	def changed(self):
		self.old_value = self.current_value
		self.current_value = self.value()
		return self.current_value != self.old_value


